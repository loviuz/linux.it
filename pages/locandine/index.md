---
layout: page
title: Locandine di pubblico dominio...
---

<p>Come associazione forniamo materiale pubblicitario per chiunque lo vuole utilizzare senza nessun limite, nemmeno a livello di diritto d'autore!</p>

<p>Il materiale può essere utilizzato per stamparlo su magliette, per storie sul tuo social network preferito e così via!</p>

* <a href="/locandine/quando-cucino" target="_blank">Quando cucino...</a>
* <a href="/locandine/formati-aperti" target="_blank">I formati aperti</a>

<a href="/stickers" class="cta-button">Chiedici di inviarti, gratuitamente, gli adesivi col logo del software libero!</a>

<p>Per saperne di più sul software libero ed in particolare su Linux, consulta l'elenco di <a href="/faq">Domande Frequenti</a>.</p>
