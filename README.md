# Sito Linux.it

Questo è il repository del sito https://www.linux.it generato staticamente con [Nikola](https://www.getnikola.com/).

## Sviluppare in locale con Docker

#### Requisiti di sviluppo per l'avvio nativo:

- docker-compose

Scarica il repository e spostati nella directory del sito:

```
git clone https://gitlab.com/ItalianLinuxSociety/linux.it && cd linux.it
```

Aggiorna il tema (solo la prima volta):

```
git submodule update --init --recursive --force
```

###  Avvio
```
docker-compose up --build

```

### Avvio (se non funziona sopra)
```
docker compose up --build
```

A questo punto visita questo indirizzo:

http://127.0.0.1:28000

Puoi cambiare la porta modificando il tuo file `.env`. Questa porta di default è stata scelta per evitarti collisioni se utilizzi altri repository Italian Linux Society. [Info](https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/DOCKER_GUIDELINES.md).

----

Se invece desideri generare solo la cartella `output` con Docker:

```
docker-compose run nikola build
```

## Senza Docker

Se invece NON desideri utilizzare Docker, vedi la prossima sezione.

### Avvio nativo

Requisiti di sviluppo per l'avvio nativo:

- una shell unix like (e.g bash, zsh,fish)
- python

Dipendenze per l'avvio nativo:

```
python3 -m pip install -r requirements.txt

git submodule update --init --recursive --force # scarica il tema di ILS

nikola build && nikola auto --browser
```

Per creare una singola build statica, eseguire questo comando dalla directory principale:

```
nikola build
```

### Avvio in Python Virtual Environment

In distribuzioni quali Arch Linux, non è possibile installare librerie per Python tramite pip, poiché il l'ambiente è gestito esternamente.
Si potrebbero installare i pacchetti uno per uno, usando la logica python-nome_pacchetto, ma è sempre consigliabile usare un ambiente virtuale Python, come ad esempio virtualenv.

Requisiti di sviluppo per l'avvio in Python Virtual Environment:

- una shell unix like (e.g bash, zsh,fish)
- python
- virtualenv

```
#Creiamo l'ambiente virtuale cloniamo il repo linux.it al suo interno
virtualenv linux.it && git -C linux.it clone https://gitlab.com/ItalianLinuxSociety/linux.it
cd linux.it/ && mv -f linux.it/{.,}* . && rm -rf linux.it

#Avviamo l'ambiente virtuale e installiamo le dipendenze necessarie con pip
source bin/activate && python3 -m python3 -m pip install --upgrade pip
pip install -r requirements.txt

#Scarichiamo il tema di ILS ed esseguiamo la build statica
git submodule update --init --recursive --force && nikola build

```

## Segnalazioni

Pagina per aprire segnalazioni e richieste pubbliche (da preferire):

https://gitlab.com/ItalianLinuxSociety/linux.it/-/issues/new

Altri contatti:

https://www.ils.org/contatti/

## Licenza

Copyright (2020-2023) contributori di Italian Linux Society e Linux.it

https://gitlab.com/ItalianLinuxSociety/linux.it/-/graphs/master

Salvo ove diversamente indicato tutti i contenuti sono rilasciati in pubblico dominio, Creative Commons Zero.

https://creativecommons.org/publicdomain/zero/1.0/

Eccezioni: loghi di associazioni, di partner e sponsor. Contattarli per conoscere le relative licenze.

Il codice sorgente invece è rilasciato sotto licenza GNU Affero General Public License.
In breve, puoi fare qualsiasi cosa, anche per scopi commerciali, a patto che rilasci le tue modifiche come software libero.

[![Agpl-3.0](https://img.shields.io/badge/License-Agpl%203.0-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.html)
